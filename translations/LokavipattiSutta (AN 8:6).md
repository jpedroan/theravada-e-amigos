## Aqui será colocada a tradução para português de 

https://www.dhammatalks.org/suttas/AN/AN8_6.html

a realizar por JPC (não verificada).

## Para já a tradução automática é:


“Monges, essas oito condições mundanas giram depois do mundo, e o mundo gira após essas oito condições mundanas. Quais oito? Ganho, perda, status, desgraça, censura, louvor, prazer e dor. Estas são as oito condições mundanas que giram depois do mundo, e o mundo gira após essas oito condições mundanas.

“Para uma pessoa comum sem instrução, surgem ganhos, perdas, status, desgraça, censura, elogios, prazer e dor. Para um discípulo instruído dos nobres, também surgem ganhos, perdas, status, desgraça, censura, louvor, prazer e dor. Então, que diferença, que distinção, que fator distintivo existe entre o discípulo bem instruído dos nobres e a pessoa comum desinstruída? ”

“Para nós, senhor, os ensinamentos têm o Abençoado como sua raiz, seu guia e seu árbitro. Seria bom se o próprio Abençoado explicasse o significado dessa afirmação. Tendo ouvido isso do Abençoado, os monges se lembrarão disso ”.

“Nesse caso, monges, ouçam e preste muita atenção. Eu vou falar."

"Como você diz, senhor", os monges responderam a ele.

O Abençoado disse: “O ganho surge para uma pessoa comum, desinstruída. Ele não reflete: 'O ganho surgiu para mim. É inconstante, estressante e sujeito a mudanças ”. Ele não a entende como aconteceu.

“Surge a perda.… O estado surge.… A desgraça surge… A censura surge… O louvor surge… O prazer surge…

“A dor surge. Ele não reflete: 'A dor surgiu para mim. É inconstante, estressante e sujeito a mudanças ”. Ele não a entende como aconteceu.

“Sua mente permanece consumida com o ganho. Sua mente permanece consumida com a perda ... com o status ... a desgraça ... a censura ... o louvor ... o prazer. Sua mente continua consumida pela dor.

“Ele acolhe o ganho surgido e se rebela contra a perda surgida. Ele saúda o status surgido e se rebela contra a desgraça surgida. Ele saúda os louvores e rebeldes surgidos contra a censura surgida. Ele acolhe o prazer surgido e se rebela contra a dor surgida. Como ele está assim envolvido em acolher e se rebelar, ele não é libertado do nascimento, envelhecimento ou morte; de tristezas, lamentações, dores, aflições ou desesperos. Ele não é libertado, digo-lhe, do sofrimento e do stress.

“Agora, o ganho surge para um discípulo bem instruído dos nobres. Ele reflete: 'O ganho surgiu para mim. É inconstante, estressante e sujeito a mudanças ”. Ele discerne como realmente é.

“Surge a perda.… O estado surge.… A desgraça surge… A censura surge… O louvor surge… O prazer surge…

“A dor surge. Ele reflete: 'A dor surgiu para mim. É inconstante, estressante e sujeito a mudanças ”. Ele discerne como realmente é.

“Sua mente não permanece consumida com o ganho. Sua mente não permanece consumida com a perda ... com o status ... a desgraça ... a censura ... o louvor ... o prazer. Sua mente não permanece consumida pela dor.

“Ele não dá as boas-vindas ao ganho surgido ou se rebela contra a perda surgida. Ele não aceita o status surgido ou se rebela contra a desgraça surgida. Ele não dá as boas-vindas ao elogio surgido, ou se revolta contra a censura surgida. Ele não acolhe o prazer surgido ou se rebela contra a dor que surge. Quando ele abandona o acolhimento e a rebelião, ele é libertado do nascimento, do envelhecimento e da morte; de tristezas, lamentações, dores, aflições e desesperos. Ele é liberado, eu lhe digo, do sofrimento e do estresse.

“Essa é a diferença, essa é a distinção, esse é o fator distintivo entre o discípulo bem instruído dos nobres e a pessoa comum sem instrução.”

Ganho / perda

status / vergonha,

censura / elogio,

prazer / dor:

Estas condições entre os seres humanos

estamos

inconstante,

impermanente,

sujeito a mudanças.

Sabendo disso, consciente, a pessoa inteligente,

pondera essas condições mutáveis.

Coisas desejáveis ​​não encantam a mente

os indesejáveis ​​não trazem resistência.

Sua acolhida

e se rebelando

estão espalhados

foi para o seu fim

não existe.

Conhecendo o estado sem poeira e sem tristeza

ele

discerne corretamente,

foi além de se tornar

para a costa mais.